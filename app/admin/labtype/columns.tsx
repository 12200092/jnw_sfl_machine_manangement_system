// "use client"

// import { EditLabType } from "./edit-labtype/edit-labtype"
// import { ColumnDef } from "@tanstack/react-table"
// import { Button } from "@/components/ui/button"
// import { MdDelete } from "react-icons/md";

// export type Payment = {
//     id: string
//     slnumber: number
//     name: string
//     description: string
// }

// export const columns: ColumnDef<Payment>[] = [
//     {
//         accessorKey: "slnumber",
//         header: "Sl No.",
//     },
//     {
//         accessorKey: "name",
//         header: "Name",
//     },
//     {
//         accessorKey: "description",
//         header:"Description"
//     },
//     {
//         accessorKey: "action",
//         header: "Action",
//         cell: (row) => (
//           <div className="space-x-1">
//             <EditLabType />
//             <Button className="bg-[#ff] border border-[#E1815B] hover:bg-[#E1815B]">
//                 <MdDelete className="text-[#E1815B] hover:text-white" size={18}/>
//             </Button>
//           </div>
//         ),
//     },

// ]

"use client";

import { ColumnDef } from "@tanstack/react-table";
// import { ViewModal } from "@/components/view-modal"
import DeleteLabtype from "./delete-labtype/delete-labtype";
import EditLabType from "./edit-labtype";

export type Payment = {
  name: string;
  description: string;
};

export const columns: ColumnDef<Payment>[] = [
  {
    accessorKey: "slnumber",
    header: "Sl No.",
  },
  {
    accessorKey: "name",
    header: "Name",
  },
  {
    accessorKey: "description",
    header: "Description",
  },
  {
    accessorKey: "action",
    header: "Action",
    cell: (row) => (
      <div className="flex gap-2">
        <EditLabType
          id={row.row.original._id}
          name={row.row.original.name}
          description={row.row.original.description}
        />
        <DeleteLabtype id={row.row.original._id} />{" "}
        {/* Pass the lab type ID to the component */}
      </div>
    ),
  },
];
