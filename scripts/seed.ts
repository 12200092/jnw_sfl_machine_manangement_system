// import mongoose from "mongoose";
// import User, { UserDocument } from "../app/models/user"; // Adjust this path as needed
// import * as bcrypt from "bcrypt";

// async function seedUser() {
//   try {
//     // Connect to MongoDB
//     await mongoose.connect("mongodb+srv://12200056gcit:cxzh9gMDF5bU1iui@mms.gv8uzmn.mongodb.net/mms_db");

//     // Seed a user
//     const userData: Partial<UserDocument> = {
//       name: "Pema Dendup",
//       email: "admin@gmail.com",
//       cid: 11504002117,
//       contact: 17665047,
//       password: "admin123",  // Plain text password
//       role: "admin",
//     };

//     // Hash the password before saving
//     const hashedPassword = await bcrypt.hash(userData.password, 10);
//     userData.password = hashedPassword;

//     // Check if user already exists
//     const existingUser = await User.findOne({ email: userData.email });
//     if (!existingUser) {
//       // Create the user
//       const user = new User(userData);
//       await user.save();
//       console.log("User seeded successfully!");
//     } else {
//       console.log("User already exists!");
//     }
//   } catch (error) {
//     console.error("Error seeding user:", error);
//   } finally {
//     // Close the MongoDB connection
//     await mongoose.disconnect();
//   }
// }

// // Seed the user
// seedUser();

import mongoose from "mongoose";
import User from "../app/models/user";
import * as bcrypt from "bcrypt";

async function seedUser() {
  try {
    await mongoose.connect("mongodb+srv://12200056gcit:cxzh9gMDF5bU1iui@mms.gv8uzmn.mongodb.net/mms_db");

    const userData = {
      name: "Pema Dendup",
      email: "admin@gmail.com",
      cid: 11504002117,
      contact: 17665047,
      password: "admin123",
      role: "admin",
    };

    const existingUser = await User.findOne({ email: userData.email });
    if (!existingUser) {
      const hashedPassword = await bcrypt.hash(userData.password, 10);
      userData.password = hashedPassword;
      const user = new User(userData);
      await user.save();
      console.log("User seeded successfully!");
    } else {
      console.log("User already exists!");
    }
  } catch (error) {
    console.error("Error seeding user:", error);
  } finally {
    await mongoose.disconnect();
  }
}

seedUser();
