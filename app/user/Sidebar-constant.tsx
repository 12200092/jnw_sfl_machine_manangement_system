import React from 'react'
import { SideNavItemGroup } from '../../components/types/types';
import { BsBookmarkCheck } from "react-icons/bs";
import { MdCategory, MdHistory } from 'react-icons/md';
import { BiCategory, BiUser, BiHomeAlt } from "react-icons/bi";
import { LuMicroscope } from "react-icons/lu";
import { GrVmMaintenance } from "react-icons/gr";


export const SIDENAV_ITEMS: SideNavItemGroup[] = [
    {
        title:'Menu',
        menuList:[
            {
                title: 'Book Machines',
                path:'/user/book-machines',
                icon: <LuMicroscope size={20}></LuMicroscope>
            },
            {
                title: 'My Bookings',
                path:'/user/my-bookings',
                icon: <BsBookmarkCheck size={20}></BsBookmarkCheck>
            },
        ]
    },
]

