'use client'
import { ColumnDef } from "@tanstack/react-table";
import { Button } from "@/components/ui/button";
import { EditModal } from "@/app/admin/machines/edit-machine";
import { ViewModal } from "@/app/admin/machines/view-machine";

export type Payment = {
  id: string;
  slnumber: number;
  name: string;
  bookeddate: string;
  bookedtime: string;
};

export const columns: ColumnDef<Payment>[] = [
  {
    accessorKey: "slnumber",
    header: "Sl No.",
  },
  {
    accessorKey: "name",
    header: "Name",
  },
  {
    accessorKey: "bookeddate",
    header: "Booked Date",
  },
  {
    accessorKey: "bookedtime",
    header: "Booked Time",
  },
  {
    accessorKey: "action",
    header: "Action",
    cell: (row) => (
        <div className="">
            <Button className="border border-[#E1815B] bg-white text-[#E1815B] hover:bg-[#E1815B] hover:text-white">Cancel Booking</Button>    
        </div>
    ),
  },
];
