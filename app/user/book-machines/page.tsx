"use client"
import React, { useState } from 'react';
import { format } from "date-fns"
import { Calendar } from "@/components/ui/calendar"
import { cn } from "@/lib/utils"
import { Button } from "@/components/ui/button"
import { IoCalendarOutline } from "react-icons/io5";
import {
  Popover,
  PopoverContent,
  PopoverTrigger,
} from "@/components/ui/popover"

export default function MachineBooking() {
  // Sample data for machines and their booking status
  const machines = [
    { id: 1, name: 'Jet Cutter', status: 'available' },
    { id: 2, name: 'Pursa 3D Printer', status: 'maintenance' },
    { id: 3, name: 'SawStop Professional 3.0HP', status: 'booked' },
  ];

  // Sample data for time intervals
  const timeIntervals = ['9-10', '10-11', '11-12', '12-1', '1-2', '2-3', '3-4', '4-5'];

  // Initial state for bookings
  const [bookings, setBookings] = useState({});
  const [selectedDate, setSelectedDate] = useState(new Date()); // Changed from date to selectedDate

  // Function to handle booking a machine
  const handleBooking = (machineId, timeInterval) => {
    // Update booking status for the selected machine and time interval
    setBookings((prevBookings) => ({
      ...prevBookings,
      [`${machineId}-${timeInterval}`]: true,
    }));
  };

  return (
    <>
      <h2 className='text-3xl font-bold tracking-tight'>Machine Booking</h2>
      <div className="flex-1 space-y-4 pt-4">
        <div className="grid gap-4 px-3 border p-4 bg-white rounded-md">
          <div className="pb-4">
            <p className='font-semibold'>Select a date: </p>
            <Popover>
              <PopoverTrigger asChild>
                <Button
                  variant={"outline"}
                  className={cn(
                    "w-[200px] justify-start text-left",
                    !selectedDate && "text-muted-foreground" // Changed from date to selectedDate
                  )}
                >
                  <IoCalendarOutline className="mr-2 h-4 w-4" />
                  {selectedDate ? format(selectedDate, "PPP") : <span>Pick a date</span>} {/* Changed from date to selectedDate */}
                </Button>
              </PopoverTrigger>
              <PopoverContent className="w-auto p-0" align="start">
                <Calendar
                  mode="single"
                  selected={selectedDate} // Changed from date to selectedDate
                  onSelect={setSelectedDate} // Changed from setDate to setSelectedDate
                  initialFocus
                />
              </PopoverContent>
            </Popover>
          </div>
          <p className='font-semibold'>Select time: </p>
          <table className="table-auto w-full">
            <thead>
              <tr>
                <th className="border px-4 py-2"></th>
                {timeIntervals.map((interval) => (
                  <th key={interval} className="border px-4 py-2">
                    {interval}
                  </th>
                ))}
              </tr>
            </thead>
            <tbody>
              {machines.map((machine) => (
                <tr key={machine.id}>
                  <td className="border text-sm px-4 py-2">{machine.name}</td>
                  {timeIntervals.map((interval) => (
                    <td
                      key={`${machine.id}-${interval}`}
                      className={`border px-4 py-2 text-center ${
                        // Set cell color based on booking status
                        bookings[`${machine.id}-${interval}`]
                          ? 'bg-red-500'
                          : machine.status === 'maintenance'
                          ? 'bg-blue-500'
                          : 'bg-green-500'
                      }`}
                      onClick={() => handleBooking(machine.id, interval)}
                    ></td>
                  ))}
                </tr>
              ))}
            </tbody>
          </table>
        </div>
      </div>
    </>
  );
}
