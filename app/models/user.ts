import mongoose, { Schema, Document } from "mongoose";
// import bcrypt from "bcrypt";

export interface UserDocument extends Document {
  name: string;
  email: string;
  cid: number;
  contact: number;
  password: string;
  role: "admin" | "user";
  machines: mongoose.Types.ObjectId[]; // Array of ObjectIds referencing the Machine model
}

const userSchema = new Schema<UserDocument>(
  {
    name: { type: String, required: true },
    email: { type: String, required: true },
    cid: { type: Number, required: true, unique: true },
    contact: { type: Number, required: true, unique: true },
    password: { type: String, required: true },
    role: {
      type: String,
      enum: ["admin", "user"],
      default: "user",
      required: true,
    },
    machines: [{ type: mongoose.Schema.Types.ObjectId, ref: "Machine" }], // New field for machines
  },
  {
    timestamps: true,
  }
);

// Middleware to hash password before saving
// userSchema.pre<UserDocument>("save", async function (next) {
//     if (!this.isModified("password")) {
//         return next();
//     }
//     try {
//         const hashedPassword = await bcrypt.hash(this.password, 10);
//         this.password = hashedPassword;
//         return next();
//     } catch (error) {
//         return next(error);
//     }
// });
const User =
  mongoose.models.User || mongoose.model<UserDocument>("User", userSchema);

export default User;
