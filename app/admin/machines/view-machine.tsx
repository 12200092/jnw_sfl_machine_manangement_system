import { Button } from "@/components/ui/button";
import { useState, useEffect } from "react";
import { useRouter } from "next/navigation";
import {
  Dialog,
  DialogContent,
  DialogHeader,
  DialogTitle,
  DialogTrigger,
} from "@/components/ui/dialog";
import { Separator } from "@/components/ui/separator";
import { MdRemoveRedEye } from "react-icons/md";

export function ViewModal({ id, name, labtype, technicianemail, description }) {
  const [newName, setNewName] = useState(name);
  const [newLabtype, setNewLabtype] = useState(labtype);
  const [newTechnicianemail, setNewTechnicianemail] = useState(technicianemail);
  const [newDescription, setNewDescription] = useState(description);
  const router = useRouter();

  const handleSubmit = async (e) => {
    e.preventDefault();
    try {
      const res = await fetch(`http://localhost:3000/api/machines/${id}`, {
        method: "PUT",
        headers: {
          "Content-Type": "application/json",
        },
        body: JSON.stringify({
          newName,
          newLabtype,
          newTechnicianemail,
          newDescription,
        }),
      });

      if (!res.ok) {
        throw new Error("Failed to update");
      } else {
        window.location.reload();
      }
    } catch (error) {
      console.error("Error updating machine:", error);
    }
  };
  return (
    <Dialog>
      <DialogTrigger asChild>
        <button className="bg-[#ff] border rounded-md p-1 border-[#E1815B] hover:bg-[#E1815B]">
          <MdRemoveRedEye
            className="text-[#E1815B] hover:text-white"
            size={18}
          />
        </button>
      </DialogTrigger>
      <DialogContent className="px-10">
        <DialogHeader>
          <DialogTitle>View Machine Details</DialogTitle>
        </DialogHeader>
        <Separator className="" />
        <form onSubmit={handleSubmit} className="flex flex-col gap-3">
          <div className="grid gap-4 py-4">
            <div className="grid items-center gap-1">
              Machine Name : {newName}
            </div>
            <div className="grid items-center gap-1">
              Lab Type : {newLabtype}
            </div>
            <div className="grid items-center gap-1">
              Maintenance Technician Email : {newTechnicianemail}
            </div>
            <div className="grid items-center gap-1">
              Description : {newDescription}
            </div>
          </div>
        </form>
      </DialogContent>
    </Dialog>
  );
}
