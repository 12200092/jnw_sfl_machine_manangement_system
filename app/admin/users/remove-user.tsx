import React from 'react';
import { Button } from "@/components/ui/button";
import { MdClose } from "react-icons/md";
import { useRouter } from 'next/navigation';

export default function RemoveUser({ id }) {
    const router = useRouter();
    const removeUser = async () => {
        const confirmed = window.confirm("Are you sure?");

        if (confirmed) {
            try {
                // Delete user from the first model
                const userRes = await fetch(`http://localhost:3000/api/users?id=${id}`, {
                    method: "DELETE",
                });

                if (!userRes.ok) {
                    throw new Error("Failed to remove user");
                }

                // // Apply changes to the second model
                // const allowMachineRes = await fetch(http://localhost:3000/api/allowmachine?id=${id}, {
                //     method: "DELETE",
                // });

                // if (!allowMachineRes.ok) {
                //     throw new Error("Failed to remove user from allowMachine");
                // }

                // If both requests are successful, reload the page or update the state
                window.location.reload();
            } catch (error) {
                console.error("Error removing user:", error);
                alert("Failed to remove user. Please try again.");
            }
        }
    };

    return (
        <Button className="bg-[#fff] border border-[#E1815B] hover:bg-[#E1815B]" onClick={removeUser}>
            <MdClose className="text-[#E1815B] hover:text-white" size={18}/>
        </Button>
    );
}