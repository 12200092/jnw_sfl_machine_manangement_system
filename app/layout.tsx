"use client";
import { Inter } from "next/font/google";
import "./globals.css";
import ToastNotifications from "@/components/toast-notification";
import ProtectedPage from "./protectedPage";

const inter = Inter({ subsets: ["latin"] });

export default function RootLayout({
  children,
}: {
  children: React.ReactNode;
}) {
  return (
    <html lang="en">
      <body className={inter.className}>
        <ProtectedPage>{children}</ProtectedPage>
        <ToastNotifications />
      </body>
    </html>
  );
}

// "use client";
// import React from "react";
// import { Inter } from "next/font/google";
// import "./globals.css";
// import ToastNotifications from "@/components/toast-notification";
// import ProtectedPage from "./protectedPage";

// const inter = Inter({ subsets: ["latin"] });

// const RootLayout: React.FC = ({ children }) => {
//   return (
//     <html lang="en">
//       <body className={inter.className}>
//         <ProtectedPage>{children}</ProtectedPage>
//         <ToastNotifications />
//       </body>
//     </html>
//   );
// };

//  RootLayout;
