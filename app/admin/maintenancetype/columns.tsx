"use client"

import { ColumnDef } from "@tanstack/react-table"
import EditMaintenanceType  from "./edit-mtype";
import DeleteMaintenancetype from "./delete-mtype";

export type Payment = {
    id: string 
    mtype: string
    description: string
}

export const columns: ColumnDef<Payment>[] = [
    {
        accessorKey: "slnumber",
        header: "Sl No.",
    },
    {
        accessorKey: "mtype",
        header: "Maintenance Type",
    },
    {
        accessorKey: "description",
        header:"Description"
    },
    {
        accessorKey: "action",
        header: "Action",
        cell: (row) => (
          <div className="space-x-1">
            <EditMaintenanceType id={row.row.original._id} mtype={row.row.original.mtype}  description={row.row.original.description}/>
            <DeleteMaintenancetype id={row.row.original._id}></DeleteMaintenancetype>
            {/* <Button className="bg-[#ff] border border-[#E1815B] hover:bg-[#E1815B]">
                <MdDelete className="text-[#E1815B] hover:text-white" size={18}/>
            </Button> */}
          </div>
        ),
      },
]
