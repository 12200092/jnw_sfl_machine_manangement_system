// "use client"
// import { MdCheck, MdClose } from "react-icons/md";
// import { ColumnDef } from "@tanstack/react-table"
// import { Button } from "@/components/ui/button"
// import { BsThreeDots } from "react-icons/bs";
// import {DropdownMenu,DropdownMenuContent,DropdownMenuItem,DropdownMenuLabel,DropdownMenuSeparator,DropdownMenuTrigger,} from "@/components/ui/dropdown-menu"
// import { EditHistory } from "./edit-history/edit-history";

// // You can use a Zod schema here if you want.
// export type Maintenance = {
//     slnumber: number
//     name: string
//     mtype: string
//     datetime: string
//     mparts: string
//     technician: string
//     duartion: string
//     status: "Done" | "Failed"
//   }

//   export const columns: ColumnDef<Maintenance>[] = [
//     {
//       accessorKey: "slnumber",
//       header: "Sl No.",
//     },
//     {
//       accessorKey: "name",
//       header: "Machine Name",
//     },
//     {
//       accessorKey: "mtype",
//       header: "Maintenance Type",
//     },
//     {
//       accessorKey: "datetime",
//       header: "Scheduled time/date",
//     },
//     {
//       accessorKey: "mparts",
//       header: "Machine Parts",
//     },
//     {
//       accessorKey: "technician",
//       header: "Maintenance Technician Email",
//     },
//     {
//       accessorKey: "duartion",
//       header: "Duration"
//     },
//     {
//       accessorKey: "status",
//       header: "Status"
//     },
//     {
//       id: "actions",
//       header: "Action",
//       enableHiding: false,
//       cell: ({ row }) => {
//         const payment = row.original
//         return (
//           <DropdownMenu>
//             <DropdownMenuTrigger asChild>
//               <Button variant="ghost" className="h-8 w-8 p-0">
//                 <span className="sr-only">Open menu</span>
//                 <BsThreeDots className="h-4 w-4" />
//               </Button>
//             </DropdownMenuTrigger>
//             <DropdownMenuContent align="end" className="py-2 px-4">
//               {/* <DropdownMenuLabel>Actions</DropdownMenuLabel> */}
//               <EditHistory/>
//               <DropdownMenuSeparator />
//               <button className="w-full rounded-md py-1 px-3 border hover:bg-accent text-sm">Delete</button>
//             </DropdownMenuContent>
//           </DropdownMenu>
//         )
//       },
//     },

//   ]

import React from "react";
import { MdCheck, MdClose } from "react-icons/md";
import { ColumnDef } from "@tanstack/react-table";
import { Button } from "@/components/ui/button";
import { BsThreeDots } from "react-icons/bs";
import {
  DropdownMenu,
  DropdownMenuContent,
  DropdownMenuSeparator,
  DropdownMenuTrigger,
} from "@/components/ui/dropdown-menu";

export type Maintenance = {
  id: string;
  slnumber: number;
  name: string;
  mtype: string;
  scheduledDate: string;
  scheduledTime: string;
  mparts: string;
  technicianemail: string;
  durationStartDate: Date;
  durationEndDate: Date;
  durationStartTime: string;
  durationEndTime: string;
  status: "Pending" | "Done" | "Failed";
};

export const columns: ColumnDef<Maintenance>[] = [
  {
    accessorKey: "slnumber",
    header: "Sl No.",
  },
  {
    accessorKey: "name",
    header: "Machine Name",
  },
  {
    accessorKey: "mtype",
    header: "Maintenance Type",
  },
  {
    accessorKey: "scheduledDateTime",
    header: "Scheduled Date/Time",
    cell: (row) => {
      const { scheduledDate, scheduledTime } = row.row.original;

      // Extract only the date portion
      const dateOnly = scheduledDate.split("T")[0];

      const formattedDateTime = `${dateOnly} / ${scheduledTime}`; // Add a slash between date and time
      return <div>{formattedDateTime}</div>;
    },
  },

  {
    accessorKey: "technicianemail",
    header: "Technician Email",
  },
  {
    accessorKey: "duration",
    header: "Duration",
    cell: (row) => {
      const {
        durationStartDate,
        durationEndDate,
        durationStartTime,
        durationEndTime,
      } = row.row.original;

      // Parse the dates and times
      const startDate = new Date(durationStartDate);
      const endDate = new Date(durationEndDate);
      const startTime = new Date(`1970-01-01T${durationStartTime}`);
      const endTime = new Date(`1970-01-01T${durationEndTime}`);

      // Calculate the difference in milliseconds
      const diffMilliseconds =
        endDate.getTime() -
        startDate.getTime() +
        endTime.getTime() -
        startTime.getTime();
      // Convert milliseconds to hours
      const durationHours = diffMilliseconds / (1000 * 60 * 60);

      return <div>{durationHours} hr(s)</div>;
    },
  },

  {
    accessorKey: "mparts",
    header: "Machine Parts",
  },
  {
    accessorKey: "status",
    header: "Status",
  },
  // {
  //   accessorKey: "status",
  //   header: "Status",
  //   cell: (row) => (
  //     <div className=" space-y-1 gap-1">
  //       <button className="bg-[#fff] border border-[#496EA4] mr-1 p-1 rounded-md hover:bg-[#496EA4]">
  //         <MdCheck className="text-[#496EA4] hover:text-white" size={18} />
  //       </button>
  //       <button className="bg-[#ff] border border-[#E1815B] p-1 rounded-md hover:bg-[#E1815B]">
  //         <MdClose className="text-[#E1815B] hover:text-white" size={18} />
  //       </button>
  //     </div>
  //   ),
  // },
  {
    id: "actions",
    header: "Action",
    enableHiding: false,
    cell: ({ row }) => {
      const payment = row.original;
      return (
        <DropdownMenu>
          <DropdownMenuTrigger asChild>
            <Button variant="ghost" className="h-8 w-8 p-0">
              <span className="sr-only">Open menu</span>
              <BsThreeDots className="h-4 w-4" />
            </Button>
          </DropdownMenuTrigger>
          <DropdownMenuContent align="end" className="py-2 px-4">
            {/* <EditMaintenance /> */}
            <DropdownMenuSeparator />
            {/* <button className="w-full rounded-md py-1 px-3 border hover:bg-accent text-sm">
              Delete
            </button> */}
            {/* <DeleteMaintenance id={row.original._id} /> */}
          </DropdownMenuContent>
        </DropdownMenu>
      );
    },
  },
];
