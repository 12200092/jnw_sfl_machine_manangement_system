// "use client"

// import { Bar, BarChart, ResponsiveContainer, XAxis, YAxis } from "recharts"

// const data = [
//   {
//     name: "Jan",
//     total: Math.floor(Math.random() * 5000) + 1000,
//   },
//   {
//     name: "Feb",
//     total: Math.floor(Math.random() * 5000) + 1000,
//   },
//   {
//     name: "Mar",
//     total: Math.floor(Math.random() * 5000) + 1000,
//   },
//   {
//     name: "Apr",
//     total: Math.floor(Math.random() * 5000) + 1000,
//   },
//   {
//     name: "May",
//     total: Math.floor(Math.random() * 5000) + 1000,
//   },
//   {
//     name: "Jun",
//     total: Math.floor(Math.random() * 5000) + 1000,
//   },
//   {
//     name: "Jul",
//     total: Math.floor(Math.random() * 5000) + 1000,
//   },
//   {
//     name: "Aug",
//     total: Math.floor(Math.random() * 5000) + 1000,
//   },
//   {
//     name: "Sep",
//     total: Math.floor(Math.random() * 5000) + 1000,
//   },
//   {
//     name: "Oct",
//     total: Math.floor(Math.random() * 5000) + 1000,
//   },
//   {
//     name: "Nov",
//     total: Math.floor(Math.random() * 5000) + 1000,
//   },
//   {
//     name: "Dec",
//     total: Math.floor(Math.random() * 5000) + 1000,
//   },
// ]

// export function Overview() {
//   return (
//     <ResponsiveContainer >
//       <BarChart data={data}>
//         <XAxis
//           dataKey="name"
//           stroke="#888888"
//           fontSize={12}
//           tickLine={false}
//           axisLine={false}
//         />
//         <YAxis
//           stroke="#888888"
//           fontSize={12}
//           tickLine={false}
//           axisLine={false}
//           tickFormatter={(value) => `$${value}`}
//         />
//         <Bar
//           dataKey="total"
//           fill="currentColor"
//           radius={[4, 4, 0, 0]}
//           className="fill-primary"
//         />
//       </BarChart>
//     </ResponsiveContainer>
//   )
// }


"use client"
import React from "react";
import {ResponsiveContainer,BarChart,Bar,XAxis,YAxis,CartesianGrid,Tooltip,} from "recharts";

const data = [
  { machine: "3D Printer", bookings: 30 },
  { machine: "Jet Cutter", bookings: 45 },
  { machine: "Machine 3", bookings: 10 },
  { machine: "Machine 4", bookings: 8 },
  { machine: "Machine 5", bookings: 10 },
  { machine: "Machine 6", bookings: 9 },
  { machine: "Machine 7", bookings: 53 },
  { machine: "Machine 8", bookings: 52 },
  { machine: "Machine 9", bookings: 79 },
  { machine: "Machine 10", bookings: 94 },
  { machine: "Machine 12", bookings: 43 },
  { machine: "Machine 13", bookings: 74 },
  { machine: "Machine 14", bookings: 71 },
  { machine: "Machine 15", bookings: 17 },
  { machine: "Machine 16", bookings: 86 },
  { machine: "Machine 17", bookings: 16 },
  { machine: "Machine 18", bookings: 25 },
  { machine: "Machine 19", bookings: 22 },
  { machine: "Machine 20", bookings: 72 },

];

export function Overview() {
  return (
    <ResponsiveContainer width="100%" height={350}>
    <BarChart
      // width={800}
      // height={350}
      data={data}>
      <CartesianGrid strokeDasharray="3 3" />
      <XAxis
        dataKey="machine"
        angle={-35}
        textAnchor="end"
        interval={0}
        height={120} // Increase the height of the x-axis
        tick={{fontSize: "12px"}} // Change the font size of x-axis labels

      />      
      <YAxis />
      <Tooltip />
      <Bar dataKey="bookings" fill="#76a8f9" />
    </BarChart>
    </ResponsiveContainer >
  );
}