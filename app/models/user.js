"use strict";
// import mongoose, { Schema } from "mongoose";
Object.defineProperty(exports, "__esModule", { value: true });
// const userSchema = new Schema(
//     {
//         name: String,
//         email: String,
//         cid: Number,
//         contact: Number,
//         password: String,
//     },
//     {
//         timestamps: true,
//     }
// );
// const User = mongoose.models.User || mongoose.model("User", userSchema);
// export default User;
var mongoose_1 = require("mongoose");
var userSchema = new mongoose_1.Schema({
    name: { type: String, required: true },
    email: { type: String, required: true, unique: true },
    cid: { type: Number, required: true, unique: true },
    contact: { type: Number, required: true, unique: true },
    password: { type: String, required: true },
    role: { type: String, enum: ['admin', 'user'], default: 'user', required: true },
}, {
    timestamps: true,
});
// Middleware to hash password before saving
// userSchema.pre<UserDocument>("save", async function (next) {
//     if (!this.isModified("password")) {
//         return next();
//     }
//     try {
//         const hashedPassword = await bcrypt.hash(this.password, 10);
//         this.password = hashedPassword;
//         return next();
//     } catch (error) {
//         return next(error);
//     }
// });
var User = mongoose_1.default.models.User || mongoose_1.default.model("User", userSchema);
exports.default = User;
