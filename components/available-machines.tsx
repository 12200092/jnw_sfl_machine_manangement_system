"use client"

import { RadialBar, RadialBarChart, ResponsiveContainer, Legend } from "recharts"

const data = [
  {
    name: "Under Maintenance",
    uv: Math.floor(Math.random() * 100) + 10,
    fill: "#413ea0",
  },
  {
    name: "Available Machines",
    uv: Math.floor(Math.random() * 100) + 20,
    fill: "#309d89",
  },
  
]

const style = {
  top: 0,
  left: 0,
  fontSize: '14px'
};

export function RadialChart() {
  return (
    <ResponsiveContainer width="100%" height={350}>
      <RadialBarChart
        innerRadius="50%"
        outerRadius="90%"
        barSize={15}
        data={data}
      >
        <RadialBar
          label={{ position: 'insideStart', fill: '#fff' }}
          background
          clockWise
          dataKey="uv"
          minAngle={55}
        />
        <Legend iconSize={10} layout="vertical" verticalAlign="bottom" wrapperStyle={style}/>
      </RadialBarChart>
    </ResponsiveContainer>
  )
}
