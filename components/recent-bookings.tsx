"use client"

import * as React from "react"
import {
  ColumnDef,
  ColumnFiltersState,
  flexRender,
  getCoreRowModel,
  getFilteredRowModel,
  getPaginationRowModel,
  useReactTable,
} from "@tanstack/react-table"
import { Button } from "@/components/ui/button"
import { Input } from "@/components/ui/input"
import {Table,TableBody,TableCell,TableHead,TableHeader,TableRow,} from "@/components/ui/table"

const data: History[] = [

    {
        id: "bhqecj4p",
        serialNo:1, 
        name:"3D Printer",
        bookedDate: "03-10-2024",
        bookedTime: "9-10am",
        bookedBy:"Tshewang"
    },
    {
        id: "bhqecj4p",
        serialNo: 2, 
        name:"3D Printer",
        bookedDate: "03-10-2024",
        bookedTime: "9-10am",
        bookedBy:"Tshewang"
    },
    {
        id: "bhqecj4p",
        serialNo: 3, 
        name:"3D Printer",
        bookedDate: "03-10-2024",
        bookedTime: "9-10am",
        bookedBy:"Tshewang"
    },
    {
        id: "bhqecj4p",
        serialNo: 4, 
        name:"3D Printer",
        bookedDate: "03-10-2024",
        bookedTime: "9-10am",
        bookedBy:"Tshewang"
    },
    {
        id: "bhqecj4p",
        serialNo: 5, 
        name:"3D Printer",
        bookedDate: "03-10-2024",
        bookedTime: "9-10am",
        bookedBy:"Tshewang"
    },
    {
        id: "bhqecj4p",
        serialNo: 6, 
        name:"Jet Cutter",
        bookedDate: "03-10-2024",
        bookedTime: "9-10am",
        bookedBy:"Tshewang"
    },
    {
        id: "ShopBot",
        serialNo: 7, 
        name:"ShopBot",
        bookedDate: "03-10-2024",
        bookedTime: "9-10am",
        bookedBy:"Tshewang"
    },
    {
        id: "bhqecj4p",
        serialNo: 6, 
        name:"Jet Cutter",
        bookedDate: "03-10-2024",
        bookedTime: "9-10am",
        bookedBy:"Tshewang"
    },
    {
        id: "ShopBot",
        serialNo: 7, 
        name:"ShopBot",
        bookedDate: "03-10-2024",
        bookedTime: "9-10am",
        bookedBy:"Tshewang"
    },
    {
        id: "bhqecj4p",
        serialNo: 6, 
        name:"Jet Cutter",
        bookedDate: "03-10-2024",
        bookedTime: "9-10am",
        bookedBy:"Tshewang"
    },
    {
        id: "ShopBot",
        serialNo: 7, 
        name:"ShopBot",
        bookedDate: "03-10-2024",
        bookedTime: "9-10am",
        bookedBy:"Tshewang"
    },
]

export type History = {
  id: string
  serialNo: number
  name: string
  bookedDate: string
  bookedTime: string
  bookedBy: string
}

export const columns: ColumnDef<History>[] = [
  {
    accessorKey:'serialNo',
    header:'Sl No.'
  },
  {
    accessorKey:'name',
    header:'Machine Name'
  },
  {
    accessorKey:'bookedDate',
    header:'Booked Date'
  },
  {
    accessorKey:'bookedTime',
    header:'Booked Time'
  },
  {
    accessorKey:'bookedBy',
    header:'Booked By'
  },
]

export function RecentHistory() {
  const [columnFilters, setColumnFilters] = React.useState<ColumnFiltersState>(
    []
  )

  const table = useReactTable({
    data,
    columns,
    initialState:{
        pagination:{
            "pageIndex":0,
            "pageSize":3
        }
    },
    onColumnFiltersChange: setColumnFilters,
    getCoreRowModel: getCoreRowModel(),
    getPaginationRowModel: getPaginationRowModel(),
    getFilteredRowModel: getFilteredRowModel(),
    state: {
      columnFilters,
    },
  })

  return (
    <div className="w-full">
      <div className="flex items-center pb-2">
        <Input
          placeholder="Filter machines..."
          value={(table.getColumn("name")?.getFilterValue() as string) ?? ""}
          onChange={(event) =>
            table.getColumn("name")?.setFilterValue(event.target.value)
          }
          className="max-w-sm"
        />       
      </div>
      <div className="rounded-md border">
        <Table>
          <TableHeader>
            {table.getHeaderGroups().map((headerGroup) => (
              <TableRow key={headerGroup.id}>
                {headerGroup.headers.map((header) => {
                  return (
                    <TableHead key={header.id}>
                      {header.isPlaceholder
                        ? null
                        : flexRender(
                            header.column.columnDef.header,
                            header.getContext()
                          )}
                    </TableHead>
                  )
                })}
              </TableRow>
            ))}
          </TableHeader>
          <TableBody>
            {table.getRowModel().rows?.length ? (
              table.getRowModel().rows.map((row) => (
                <TableRow
                  key={row.id}
                  data-state={row.getIsSelected() && "selected"}
                >
                  {row.getVisibleCells().map((cell) => (
                    <TableCell key={cell.id}>
                      {flexRender(
                        cell.column.columnDef.cell,
                        cell.getContext()
                      )}
                    </TableCell>
                  ))}
                </TableRow>
              ))
            ) : (
              <TableRow>
                <TableCell
                  colSpan={columns.length}
                  className="h-24 text-center"
                >
                  No results.
                </TableCell>
              </TableRow>
            )}
          </TableBody>
        </Table>
      </div>
      <div className="flex items-center justify-end space-x-2 py-4">
        <div className="flex-1 text-sm text-muted-foreground">
          Page {table.getState().pagination.pageIndex + 1} of{" "}
          {table.getPageCount()}
        </div>
        <div className="space-x-2">
          <Button
            variant="outline"
            size="sm"
            onClick={() => table.previousPage()}
            disabled={!table.getCanPreviousPage()}
          >
            Previous
          </Button>
          <Button
            variant="outline"
            size="sm"
            onClick={() => table.nextPage()}
            disabled={!table.getCanNextPage()}
          >
            Next
          </Button>
        </div>
      </div>
    </div>
  )
}
