"use client";

import { useState, useEffect } from "react";
import axios from "axios";
import DatePicker from "react-datepicker";
import "react-datepicker/dist/react-datepicker.css";
import { Button } from "@/components/ui/button";
import {
  Dialog,
  DialogContent,
  DialogFooter,
  DialogHeader,
  DialogTitle,
  DialogTrigger,
} from "@/components/ui/dialog";
import { Input } from "@/components/ui/input";
import { Label } from "@/components/ui/label";
import {
  Select,
  SelectContent,
  SelectItem,
  SelectTrigger,
  SelectValue,
} from "@/components/ui/select";
import { Separator } from "@/components/ui/separator";

const ScheduleMaintenance = () => {
  const [name, setName] = useState("");
  const [mtype, setMtype] = useState("");
  const [maintenanceTypes, setMtypes] = useState([]);
  const [machineNames, setMachineNames] = useState([]);
  const [scheduledDate, setScheduledDate] = useState(new Date());
  const [scheduledTime, setScheduledTime] = useState(new Date());
  const [mparts, setMparts] = useState("");
  const [technicianemail, setTechnicianemail] = useState("");
  const [isOpen, setIsOpen] = useState(false);

  useEffect(() => {
    async function fetchMaintenanceTypes() {
      try {
        const response = await axios.get(
          "http://localhost:3000/api/maintenancetype"
        );
        setMtypes(response.data.maintenancetypes);
      } catch (error) {
        console.error("Error fetching maintenance types:", error);
      }
    }
    fetchMaintenanceTypes();
  }, []);

  useEffect(() => {
    async function fetchMachineNames() {
      try {
        const response = await axios.get("http://localhost:3000/api/machines");
        setMachineNames(response.data.machines);
      } catch (error) {
        console.error("Error fetching machine names:", error);
      }
    }
    fetchMachineNames();
  }, []);

  const handleSubmit = async () => {
    try {
      await axios.post("http://localhost:3000/api/maintenance", {
        name,
        mtype,
        scheduledDate: scheduledDate.toISOString().split("T")[0],
        scheduledTime: scheduledTime.toTimeString().split(" ")[0],
        mparts,
        technicianemail,
      });
      setIsOpen(false);
      // Reset form fields
      setName("");
      setMtype("");
      setScheduledDate(new Date());
      setScheduledTime(new Date());
      setMparts("");
      setTechnicianemail("");
      // Additional actions after submission
    } catch (error) {
      console.error("Error adding maintenance:", error);
    }
  };

  return (
    <Dialog isOpen={isOpen} onDismiss={() => setIsOpen(false)}>
      <DialogTrigger asChild>
        <Button className="bg-[#E1815B] bg-opacity-5 border-2 border-[#E1815B] px-4 text-[#E1815B] hover:text-[#E1815B] font-bold">
          Schedule Maintenance
        </Button>
      </DialogTrigger>
      <DialogContent className="px-10">
        <DialogHeader>
          <DialogTitle>Schedule Maintenance</DialogTitle>
        </DialogHeader>
        <Separator className="" />
        <div
          className="max-h-[26rem] overflow-y-auto p-1"
          style={{ scrollbarWidth: "thin", scrollbarColor: "#555 #f1f1f1" }}
        >
          <div className="grid gap-4">
            <div className="grid items-center gap-1">
              <Label htmlFor="name" className="">
                Machine Name :
              </Label>
              <Select value={name} onValueChange={setName}>
                <SelectTrigger className="w-full">
                  <SelectValue />
                </SelectTrigger>
                <SelectContent>
                  {machineNames.map((machine) => (
                    <SelectItem key={machine.id} value={machine.name}>
                      {machine.name}
                    </SelectItem>
                  ))}
                </SelectContent>
              </Select>
            </div>
            <div className="grid items-center gap-1">
              <Label htmlFor="mtype" className="">
                Maintenance Type :
              </Label>
              <Select value={mtype} onValueChange={setMtype}>
                <SelectTrigger className="w-full">
                  <SelectValue />
                </SelectTrigger>
                <SelectContent>
                  {maintenanceTypes.map((type) => (
                    <SelectItem key={type.id} value={type.mtype}>
                      {type.mtype}
                    </SelectItem>
                  ))}
                </SelectContent>
              </Select>
            </div>
            <div className="flex flex-col">
              <Label htmlFor="scheduledDate" className="">
                Scheduled Date:
              </Label>
              <DatePicker
                selected={scheduledDate}
                onChange={(date) => setScheduledDate(date)}
                dateFormat="yyyy-MM-dd"
                className="input border p-1 rounded-md w-full text-xs"
              />
            </div>
            <div className="flex flex-col">
              <Label htmlFor="scheduledTime" className="">
                Scheduled Time:
              </Label>
              <DatePicker
                selected={scheduledTime}
                onChange={(time) => setScheduledTime(time)}
                showTimeSelect
                showTimeSelectOnly
                dateFormat="HH:mm"
                className="input border p-1 rounded-md w-full text-xs"
              />
            </div>
            <div className="grid items-center gap-1">
              <Label htmlFor="mparts" className="">
                Machine Parts :
              </Label>
              <Input
                id="mparts"
                className=""
                value={mparts}
                onChange={(e) => setMparts(e.target.value)}
              />
            </div>
            <div className="grid items-center gap-1">
              <Label htmlFor="technicianemail" className="">
                Maintenance Technician Email :
              </Label>
              <Input
                id="technicianemail"
                className=""
                value={technicianemail}
                onChange={(e) => setTechnicianemail(e.target.value)}
              />
            </div>
          </div>
        </div>
        <Separator className="" />
        <DialogFooter>
          <form
            onSubmit={(e) => {
              e.preventDefault();
              handleSubmit();
            }}
          >
            <Button
              type="submit"
              className="bg-[#E1815B] text-white font-bold px-8"
            >
              Schedule Maintenance
            </Button>
          </form>
        </DialogFooter>
      </DialogContent>
    </Dialog>
  );
}

export default ScheduleMaintenance;
