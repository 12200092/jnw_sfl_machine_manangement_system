// import * as React from "react"
// import { DataTable } from "./data-table"
// import { Tabs, TabsContent, TabsList, TabsTrigger } from "@/components/ui/tabs"
// import { Button } from "@/components/ui/button"
// import { Payment, columns } from "./columns"
// import { AddMachine } from "./add-machine/add-machine"


// async function getData(): Promise<Payment[]> {
//   // Fetch data from your API here.
//   return [
//     {
//       id: "728ed52f",
//       slnumber: 1,
//       name: "3D Printer",
//       email: "m@example.com",
//       labtype:'electronic',
//       description: "This is an example description for this ...",
//       status: "Active",
//     },
//     {
//       id: "728ed52f",
//       slnumber: 1,
//       name: "Jet Cutter",
//       email: "m@example.com",
//       labtype:'carpentry',

//       description: "This is an example description for this ...",
//       status: "Inactive",
//     },
//     {
//       id: "728ed52f",
//       slnumber: 1,
//       name: "ShopBot",
//       email: "m@example.com",
//       labtype:'carpentry',
//       description: "This is an example description for this ...",
//       status: "Active",
//     },
//     {
//       id: "728ed52f",
//       slnumber: 1,
//       name: "Pursa 3D Printer",
//       email: "m@example.com",
//       labtype:'electronic',
//       description: "This is an example description for this ...",
//       status: "Active",
//     },
//     {
//       id: "728ed52f",
//       slnumber: 1,
//       name: "Jet Cutter",
//       email: "m@example.com",
//       labtype:'electronic',
//       description: "This is an example description for this ...",
//       status: "Active",
//     },
//     {
//       id: "728ed52f",
//       slnumber: 1,
//       name: "Jet Cutter",
//       email: "m@example.com",
//       labtype:'heavy',
//       description: "This is an example description for this ...",
//       status: "Active",
//     },
//   ]
// }

// export default async function DemoPage() {
//   const data = await getData()

//   return (
//     <>
//       <h2 className='text-3xl font-bold tracking-tight '>Machines</h2>
//       <div className="flex-1 space-y-4 pt-4">
//         <div className="grid gap-4 px-3 border p-4 bg-white rounded-md">    
//           <Tabs defaultValue="overview">
//             <div className="flex items-center justify-between ">
//               <TabsList className="bg-[#E1815B] bg-opacity-10">
//                 <TabsTrigger value="overview">Carpentry</TabsTrigger>
//                 <TabsTrigger value="analytics">Electronic</TabsTrigger>
//                 <TabsTrigger value="reports">Laser</TabsTrigger>
//                 <TabsTrigger value="notifications">Heavy</TabsTrigger>
//               </TabsList>     
//             </div>
//             <TabsContent value="overview" className="">
//               <div className="">
//                 <DataTable columns={columns} data={data} />
//               </div>
//             </TabsContent>
//           </Tabs>
//         </div>
//       </div>
//     </>
//   )
// }

"use client"
import React, { useState, useEffect, useRef } from 'react';
import { DataTable } from "./data-table";
import { Tabs, TabsContent, TabsList, TabsTrigger } from "@/components/ui/tabs";
import { Payment, columns } from "./columns";
import axios from 'axios';

// Utility function to truncate text
const truncateText = (text, maxLength) => {
  if (text.length <= maxLength) return text;
  return text.slice(0, maxLength) + '...';
};

export default function DemoPage() {
  const [machines, setMachines] = useState<Payment[]>([]);
  const [labTypes, setLabTypes] = useState<string[]>([]);
  const [activeTab, setActiveTab] = useState<string>("");

  useEffect(() => {
    async function fetchData() {
      try {
        // Fetch lab types
        const labTypesResponse = await axios.get('http://localhost:3000/api/labtype');
        const labTypesData = labTypesResponse.data.labtypes.map((labType) => labType.name);
        setLabTypes(labTypesData);

        // Set the active tab to the first lab type fetched from the database
        if (labTypesData.length > 0) {
          setActiveTab(labTypesData[0]);
        }
        // Fetch machines
        const machinesResponse = await axios.get('http://localhost:3000/api/machines');
        const machinesData = machinesResponse.data.machines.map((machine, index) => ({ 
          ...machine, 
          slnumber: index + 1,
          description: truncateText(machine.description, 20) // Truncate description to 100 characters
        }));
        setMachines(machinesData);
      } catch (error) {
        console.error('Error fetching data:', error);
      }
    }
    fetchData();
  }, []);

  return (
    <>
      <h2 className='text-2xl font-bold tracking-tight'>Machines</h2>
      <div className="flex-1 space-y-4 pt-4">
        <div className="grid px-3 pt-4 border bg-white rounded-md">    
          <Tabs value={activeTab}>
            <div className="flex items-center justify-between">
              <TabsList className="bg-[#E1815B] bg-opacity-10">
                {labTypes.map((labType) => (
                  <TabsTrigger
                    key={labType}
                    value={labType}
                    onClick={() => setActiveTab(labType)} // Set the active tab on click
                  >
                    {labType}
                  </TabsTrigger>
                ))}
              </TabsList>
              {/* <div className="flex items-center space-x-2">
                <AddMachine/>
              </div> */}
            </div>
            {labTypes.map((labType) => (
              <TabsContent key={labType} value={labType} className="">
                {activeTab === labType && (
                  <div className="pt-2">
                    <DataTable 
                      columns={columns} 
                      data={machines
                        .filter(machine => machine.labtype === labType)
                        .map((machine, index) => ({ ...machine, slnumber: index + 1 }))} 
                    />
                  </div>
                )}
              </TabsContent>
            ))}
          </Tabs>
        </div>
      </div>
    </>
  );
}
