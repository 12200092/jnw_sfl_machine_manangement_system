import * as React from "react"
import { DataTable } from "./datatable"
import { Tabs, TabsContent, TabsList, TabsTrigger } from "@/components/ui/tabs"
import { Button } from "@/components/ui/button"
import { Payment, columns } from "./columns"


async function getData(): Promise<Payment[]> {
  // Fetch data from your API here.
  return [
    {
      id: "728ed52f",
      slnumber: 1,
      name: "3D Printer",
      bookeddate: "m@example.com",
      bookedtime: "This is an example description for this ...",
    },
    {
        id: "728ed52f",
        slnumber: 1,
        name: "3D Printer",
        bookeddate: "m@example.com",
        bookedtime: "This is an example description for this ...",
    },
    {
        id: "728ed52f",
        slnumber: 1,
        name: "3D Printer",
        bookeddate: "m@example.com",
        bookedtime: "This is an example description for this ...",
    },
    {
        id: "728ed52f",
        slnumber: 1,
        name: "3D Printer",
        bookeddate: "m@example.com",
        bookedtime: "This is an example description for this ...",
    },

  ]
}

export default async function MyBookings() {
  const data = await getData()

  return (
    <>
      <h2 className='text-3xl font-bold tracking-tight '>My Bookings</h2>
      <div className="flex-1 space-y-4 pt-4">
        <div className="grid gap-4 px-3 border p-4 bg-white rounded-md">    
          <Tabs defaultValue="overview">
            <div className="flex items-center justify-between ">
              <TabsList className="bg-[#E1815B] bg-opacity-10">
                <TabsTrigger value="overview">Carpentry</TabsTrigger>
                <TabsTrigger value="analytics">Electronic</TabsTrigger>
                <TabsTrigger value="reports">Laser</TabsTrigger>
                <TabsTrigger value="notifications">Heavy</TabsTrigger>
              </TabsList>
            </div>
            <TabsContent value="overview" className="">
              <div className="pt-2">
                <DataTable columns={columns} data={data} />
              </div>
            </TabsContent>
          </Tabs>
        </div>
      </div>
    </>
  )
}
