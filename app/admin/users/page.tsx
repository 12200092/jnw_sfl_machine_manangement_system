"use client";
import * as React from "react";
import { DataTable } from "./data-table";
import { Tabs, TabsContent, TabsList, TabsTrigger } from "@/components/ui/tabs";
import { NewUser, columns } from "./columns";

async function getData(): Promise<NewUser[]> {
  const response = await fetch("http://localhost:3000/api/users"); // Assuming your API endpoint is '/api/users'
  const data = await response.json();
  return data.users.map((user, index) => ({ ...user, slnumber: index + 1 }));
}

export default function DemoPage() {
  const [data, setData] = React.useState<NewUser[]>([]);

  React.useEffect(() => {
    async function fetchData() {
      const userData = await getData();
      setData(userData);
    }
    fetchData();
  }, []);

  return (
    <>
      <h2 className="text-3xl font-bold tracking-tight">Users</h2>
      <div className="flex-1 space-y-4 pt-4">
        <div className="grid gap-4 px-3 border p-4 bg-white rounded-md">
          <Tabs defaultValue="New-users">
            {/* <div className="flex items-center justify-between">
              <TabsList className="bg-[#E1815B] bg-opacity-10">
                <TabsTrigger value="Users">Users</TabsTrigger>
                <TabsTrigger value="New-users">New Users</TabsTrigger>
              </TabsList>
            </div> */}
            <TabsContent value="New-users" className="">
              <div className="pt-2">
                <DataTable columns={columns} data={data} />
              </div>
            </TabsContent>
          </Tabs>
        </div>
      </div>
    </>
  );
}
