// import connectMongoDB from "@/lib/mongodb";
// import { NextResponse } from "next/server";
// import allowMachine from "@/app/models/allowmachine";

// export async function POST(request) {
//     const { name, email, cid, contact, password, machines } = await request.json();
//     await connectMongoDB();
//     // Store the user with the hashed password
//     await allowMachine.create({ name, email, cid, contact, password, machines});
//     return NextResponse.json({ message: "User Allowed" }, { status: 201 });
// }

// export async function GET() {
//     await connectMongoDB();
//     const allMachines = await allowMachine.find();
//     return NextResponse.json({allMachines});
// }

// export async function DELETE(request){
//     const id = request.nextUrl.searchParams.get("id");
//     await connectMongoDB();
//     await allowMachine.findByIdAndDelete(id);
//     return NextResponse.json({ message: "User Suspended" }, { status: 200 });
// }

import connectMongoDB from "@/lib/mongodb";
import { NextResponse } from "next/server";
import allowMachine from "@/app/models/allowmachine";

// export async function POST(request) {
//     const { name, email, cid, contact, password, machines } = await request.json();
//     await connectMongoDB();

//     // const hashedPassword = await bcrypt.hash(password, 10); // Hash the password
//     await allowMachine.create({ name, email, cid, contact, password, machines });

//     return NextResponse.json({ message: "User Allowed" }, { status: 201 });
// }

// api/allowMachine.js

export async function POST(request) {
  const { name, email, cid, contact, password, machines } =
    await request.json();
  await connectMongoDB();

  try {
    await allowMachine.create({
      name,
      email,
      cid,
      contact,
      password,
      machines,
    });
    return NextResponse.json({ message: "User Allowed" }, { status: 201 });
  } catch (error) {
    return NextResponse.error(error, { status: 500 });
  }
}

export async function GET() {
  await connectMongoDB();
  const allMachines = await allowMachine.find();
  return NextResponse.json({ allMachines });
}

export async function DELETE(request) {
  const id = request.nextUrl.searchParams.get("id");
  await connectMongoDB();
  await allowMachine.findByIdAndDelete(id);
  return NextResponse.json({ message: "User Suspended" }, { status: 200 });
}
