import { Button } from "@/components/ui/button"
import {Dialog,DialogContent,DialogFooter,DialogHeader,DialogTitle,DialogTrigger,} from "@/components/ui/dialog"
import { Input } from "@/components/ui/input"
import { Label } from "@/components/ui/label"
import {Select,SelectContent,SelectItem,SelectTrigger,SelectValue,} from "@/components/ui/select"
import {
  Form,
  FormControl,
  FormDescription,
  FormField,
  FormItem,
  FormLabel,
  FormMessage,
} from "@/components/ui/form"
import { Separator } from "@/components/ui/separator"
import { MdModeEdit } from "react-icons/md"
import { Textarea } from "@/components/ui/textarea"

export function EditHistory() {
  return (
    <Dialog>
      <DialogTrigger asChild>
        <button className="w-full rounded-md py-1 px-3 border hover:bg-accent text-sm">Edit
        </button>
      </DialogTrigger>
      <DialogContent className="px-10">
        <DialogHeader>
          <DialogTitle>Edit Maintenance History</DialogTitle>
        </DialogHeader>
        <Separator className="" />
        <div className="grid gap-4">
          <div className="grid items-center gap-1">
            <Label htmlFor="name" className="">
              Machine Name :
            </Label>
            <Input id="name" className="" />
          </div>
          <div className="grid items-center gap-1">
            <Label htmlFor="username" className="">
              Maintenance Type :
            </Label>
            <Select>
              <SelectTrigger className="w-full">
                <SelectValue />
              </SelectTrigger>
              <SelectContent>
                <SelectItem value="Carpentry">Carpentry</SelectItem>
                <SelectItem value="Electronic">Electronic</SelectItem>
                <SelectItem value="Laser">Laser</SelectItem>
                <SelectItem value="Heavy">Heavy</SelectItem>
              </SelectContent>
            </Select>
          </div>
          <div className="grid items-center gap-1">
            <Label htmlFor="name" className="">
              Scheduled date/time :
            </Label>
            <Input id="name" className="" />
          </div>
          <div className="grid items-center gap-1">
            <Label htmlFor="name" className="">
              Machine Part:
            </Label>
            <Input id="name" className="" />
          </div>
          <div className="grid items-center gap-1">
            <Label htmlFor="name" className="">
              Maintenance Technician Email :
            </Label>
            <Input id="name" className="" />
          </div>
          <div className="grid items-center gap-1">
            <Label htmlFor="name" className="">
              Duration :
            </Label>
            <Input id="name" className="" />
          </div>
        </div>
        <DialogFooter>
          <Button type="submit">Save Changes</Button>
        </DialogFooter>
      </DialogContent>
    </Dialog>
  )
}
