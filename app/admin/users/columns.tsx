"use client";
import { ColumnDef } from "@tanstack/react-table";
import { AllowMachine } from "./allow-machine";
import { MdCheck, MdClose } from "react-icons/md";
import { Button } from "@/components/ui/button";
import RemoveUser from "./remove-user";

export type NewUser = {
  id: string;
  name: string;
  email: string;
  contact: number;
  cid: number;
};

export const columns: ColumnDef<NewUser>[] = [
  {
    accessorKey: "slnumber",
    header: "Sl No.",
  },
  {
    accessorKey: "name",
    header: "Name",
  },
  {
    accessorKey: "email",
    header: "Email",
  },
  {
    accessorKey: "contact",
    header: "Contact",
  },
  {
    accessorKey: "cid",
    header: "CID",
  },
  {
    accessorKey: "action",
    header: "Action",
    cell: ({ row }) => (
      <div className="space-x-2">
        <AllowMachine user={row.original} />
        <RemoveUser id={row.original._id} />
      </div>
    ),
  },
];
